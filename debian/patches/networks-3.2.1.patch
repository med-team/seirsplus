Description: fix uses of obsolete adj_matrix.

Author: Étienne Mollier <emollier@debian.org>
Bug-Debian: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1071264
Forwarded: https://github.com/ryansmcgee/seirsplus/pull/63
Last-Update: 2024-05-29
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/seirsplus/legacy_models.py
+++ b/seirsplus/legacy_models.py
@@ -749,7 +749,7 @@ class SEIRSNetworkModel():
         if type(new_G)==numpy.ndarray:
             self.A = scipy.sparse.csr_matrix(new_G)
         elif type(new_G)==networkx.classes.graph.Graph:
-            self.A = networkx.adj_matrix(new_G) # adj_matrix gives scipy.sparse csr_matrix
+            self.A = networkx.adjacency_matrix(new_G) # adjacency_matrix gives scipy.sparse csr_matrix
         else:
             raise BaseException("Input an adjacency matrix or networkx object only.")
 
@@ -766,7 +766,7 @@ class SEIRSNetworkModel():
         if type(new_Q)==numpy.ndarray:
             self.A_Q = scipy.sparse.csr_matrix(new_Q)
         elif type(new_Q)==networkx.classes.graph.Graph:
-            self.A_Q = networkx.adj_matrix(new_Q) # adj_matrix gives scipy.sparse csr_matrix
+            self.A_Q = networkx.adjacency_matrix(new_Q) # adjacency_matrix gives scipy.sparse csr_matrix
         else:
             raise BaseException("Input an adjacency matrix or networkx object only.")
 
@@ -1332,4 +1332,4 @@ class SEIRSNetworkModel():
 #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ No newline at end of file
+#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
--- a/seirsplus/models.py
+++ b/seirsplus/models.py
@@ -722,7 +722,7 @@ class SEIRSNetworkModel():
         if type(self.G)==numpy.ndarray:
             self.A = scipy.sparse.csr_matrix(self.G)
         elif type(self.G)==networkx.classes.graph.Graph:
-            self.A = networkx.adj_matrix(self.G) # adj_matrix gives scipy.sparse csr_matrix
+            self.A = networkx.adjacency_matrix(self.G) # adjacency_matrix gives scipy.sparse csr_matrix
         else:
             raise BaseException("Input an adjacency matrix or networkx object only.")
         self.numNodes   = int(self.A.shape[1])
@@ -736,7 +736,7 @@ class SEIRSNetworkModel():
         if type(self.G_Q)==numpy.ndarray:
             self.A_Q = scipy.sparse.csr_matrix(self.G_Q)
         elif type(self.G_Q)==networkx.classes.graph.Graph:
-            self.A_Q = networkx.adj_matrix(self.G_Q) # adj_matrix gives scipy.sparse csr_matrix
+            self.A_Q = networkx.adjacency_matrix(self.G_Q) # adjacency_matrix gives scipy.sparse csr_matrix
         else:
             raise BaseException("Input an adjacency matrix or networkx object only.")
         self.numNodes_Q   = int(self.A_Q.shape[1])
@@ -1948,7 +1948,7 @@ class ExtSEIRSNetworkModel():
         if type(self.G)==numpy.ndarray:
             self.A = scipy.sparse.csr_matrix(self.G)
         elif type(self.G)==networkx.classes.graph.Graph:
-            self.A = networkx.adj_matrix(self.G) # adj_matrix gives scipy.sparse csr_matrix
+            self.A = networkx.adjacency_matrix(self.G) # adjacency_matrix gives scipy.sparse csr_matrix
         else:
             raise BaseException("Input an adjacency matrix or networkx object only.")
         self.numNodes   = int(self.A.shape[1])
@@ -1962,7 +1962,7 @@ class ExtSEIRSNetworkModel():
         if type(self.G_Q)==numpy.ndarray:
             self.A_Q = scipy.sparse.csr_matrix(self.G_Q)
         elif type(self.G_Q)==networkx.classes.graph.Graph:
-            self.A_Q = networkx.adj_matrix(self.G_Q) # adj_matrix gives scipy.sparse csr_matrix
+            self.A_Q = networkx.adjacency_matrix(self.G_Q) # adjacency_matrix gives scipy.sparse csr_matrix
         else:
             raise BaseException("Input an adjacency matrix or networkx object only.")
         self.numNodes_Q   = int(self.A_Q.shape[1])
--- a/seirsplus/networks.py
+++ b/seirsplus/networks.py
@@ -47,7 +47,7 @@ def generate_workplace_contact_network(n
     # Establish inter-cohort contacts:
     #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
-    cohortsAdjMatrices = [networkx.adj_matrix(cohortNetwork) for cohortNetwork in cohortNetworks]
+    cohortsAdjMatrices = [networkx.adjacency_matrix(cohortNetwork) for cohortNetwork in cohortNetworks]
 
     workplaceAdjMatrix = scipy.sparse.block_diag(cohortsAdjMatrices)
     workplaceNetwork   = networkx.from_scipy_sparse_matrix(workplaceAdjMatrix)
@@ -442,19 +442,19 @@ def generate_demographic_contact_network
                         print(layerGroup+" public mean degree = "+str((meanDegree)))
                         print(layerGroup+" public max degree  = "+str((maxDegree)))
 
-                    adjMatrices.append(networkx.adj_matrix(layerInfo['graph']))
+                    adjMatrices.append(networkx.adjacency_matrix(layerInfo['graph']))
 
                     # Create an adjacency matrix mask that will zero out all public edges
                     # for any isolation groups but allow all public edges for other groups:
                     if(layerGroup in isolation_groups):
-                        adjMatrices_isolation_mask.append(numpy.zeros(shape=networkx.adj_matrix(layerInfo['graph']).shape))
+                        adjMatrices_isolation_mask.append(numpy.zeros(shape=networkx.adjacency_matrix(layerInfo['graph']).shape))
                     else:
-                        # adjMatrices_isolation_mask.append(numpy.ones(shape=networkx.adj_matrix(layerInfo['graph']).shape))
+                        # adjMatrices_isolation_mask.append(numpy.ones(shape=networkx.adjacency_matrix(layerInfo['graph']).shape))
                         # The graph layer we just created represents the baseline (no dist) public connections;
                         # this should be the superset of all connections that exist in any modification of the network,
                         # therefore it should work to use this baseline adj matrix as the mask instead of a block of 1s 
                         # (which uses unnecessary memory to store a whole block of 1s, ie not sparse)
-                        adjMatrices_isolation_mask.append(networkx.adj_matrix(layerInfo['graph']))
+                        adjMatrices_isolation_mask.append(networkx.adjacency_matrix(layerInfo['graph']))
 
                     graph_generated = True
 
@@ -535,7 +535,7 @@ def generate_demographic_contact_network
         # and create graphs corresponding to the isolation intervention for each distancing level:
         #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
         for graphName, graph in graphs.items():
-            A_withIsolation = scipy.sparse.csr_matrix.multiply( networkx.adj_matrix(graph), A_isolation_mask )
+            A_withIsolation = scipy.sparse.csr_matrix.multiply( networkx.adjacency_matrix(graph), A_isolation_mask )
             graphs[graphName+'_isolation'] = networkx.from_scipy_sparse_matrix(A_withIsolation)
 
     
@@ -589,7 +589,7 @@ def generate_demographic_contact_network
             print(graphName+": number of connected components = {0:d}".format(numConnectedComps))
             print(graphName+": largest connected component = {0:d}".format(len(largestConnectedComp)))
             for layerGroup, layerInfo in layer_info.items():
-                nodeDegrees_group = networkx.adj_matrix(graph)[min(layerInfo['indices']):max(layerInfo['indices']), :].sum(axis=1)
+                nodeDegrees_group = networkx.adjacency_matrix(graph)[min(layerInfo['indices']):max(layerInfo['indices']), :].sum(axis=1)
                 print("\t"+graphName+": "+layerGroup+" final graph mean degree = "+str(numpy.mean(nodeDegrees_group)))
                 print("\t"+graphName+": "+layerGroup+" final graph max degree  = "+str(numpy.max(nodeDegrees_group)))
                 pyplot.hist(nodeDegrees_group, bins=range(int(max(nodeDegrees_group))), alpha=0.5, label=layerGroup)
